//
//  ContactusViewController.swift
//  RealWorld
//
//  Created by MacBook Pro on 2/9/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class ContactusViewController: UIViewController {

    
    @IBOutlet weak var lbl_contact: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_email: UILabel!
    @IBOutlet weak var lbl_sleepline: UILabel!
    @IBOutlet weak var lbl_website: UILabel!
    
    @IBOutlet weak var view_height: NSLayoutConstraint!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "ic_sidemenu"), for: .normal)
        button.layer.cornerRadius = 5
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.addTarget(self, action: #selector(Tapped_Sidemenu), for: .touchUpInside)
        
        let lbl:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        lbl.font = UIFont.init(name: "Stilu-SemiBold", size: 15)
        lbl.text = "Contact Us"
        lbl.textColor = UIColor.black
        let bar_buttonlbl = UIBarButtonItem(customView: lbl)
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.leftBarButtonItems = [barButton,bar_buttonlbl]
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        
        self.SetAddress()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func Tapped_Sidemenu() {
        self.slideMenuController()?.toggleLeft()
    }
    
    func SetAddress() {
        
        self.lbl_address.text = "A-103, Safal Solitar, Near Divya Bhasker Press, SG Highway, Ahmedabad, Gujarat -380015."
        self.lbl_email.text = "info@realworld.co.in"
        self.lbl_contact.text = "Telephone: 9724771729 | 02717-450045"
        self.lbl_website.text = "www.realworld.co.in"
        self.view_height.constant = self.lbl_sleepline.frame.origin.y+10
    }
}
