//
//  ContainerViewController.swift
//  LetsGo
//
//  Created by Ritesh Shah on 12/04/17.
//  Copyright © 2017 Ritesh Shah. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class ContainerViewController: SlideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()
     
    }
	
	override func awakeFromNib() {
		
		if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") {
            
            let nav:UINavigationController = UINavigationController(rootViewController: controller)
			self.mainViewController = nav
		}
		
        
		if let controller = self.storyboard?.instantiateViewController(withIdentifier: "SidemenuViewController") {
			self.leftViewController = controller
		}
		
		super.awakeFromNib()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
