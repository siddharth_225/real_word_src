//
//  AboutusViewController.swift
//  RealWorld
//
//  Created by MacBook Pro on 2/9/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class AboutusViewController: UIViewController {

    @IBOutlet weak var lbl_vision: UILabel!
    
    @IBOutlet weak var lbl_sleepline: UILabel!
    @IBOutlet weak var lbl_mission: UILabel!
    @IBOutlet weak var lbl_Description: UILabel!
    @IBOutlet weak var view_height: NSLayoutConstraint!
    
    @IBOutlet weak var view_vision: UIView!
    @IBOutlet weak var view_mision: UIView!
    @IBOutlet weak var view_aboutus: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "ic_sidemenu"), for: .normal)
        button.layer.cornerRadius = 5
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.addTarget(self, action: #selector(Tapped_Sidemenu), for: .touchUpInside)
        
        let lbl:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        lbl.font = UIFont.init(name: "Stilu-SemiBold", size: 15)
        lbl.text = "About Us"
        lbl.textColor = UIColor.black
        let bar_buttonlbl = UIBarButtonItem(customView: lbl)
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.leftBarButtonItems = [barButton,bar_buttonlbl]
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
   
        self.SetViewShadow(view: view_aboutus)
        self.SetViewShadow(view: view_mision)
        self.SetViewShadow(view: view_vision)
        
        lbl_Description.text = "Real World Industries Limited Industries Limited provides a wide range of real estates services which includes Selling Property, Renting/ Leasing Property, PG Accommodation, Construction Services.\n\nReal World Industries Limited Industries Limited is reputed and is one of the fastest growing company in Ahmedabad which renders Real Estate solutions."
        
         lbl_mission.text = "The mission of Mary-am Group is to provide excellence and quality through innovation, integrity, and a forward-thinking approach. We live by this everyday, engaging with diversity, utilizing technology,and innovation through collaboration in order to excel industry standards."
        
         lbl_vision.text = "Real World Group’s vision is to be an industry leader in every business venture and to build customer and stakeholder value. Our organization aspires to change the way you think about the service industry. Superior customer service, innovation through collaboration, and always striving for excellence and quality will give you the best experience."
        // Do any additional setup after loading the view.
    }

  override  func viewDidAppear(_ animated: Bool) {
        self.view_height.constant = self.lbl_sleepline.frame.origin.y+10
    }
    
    func SetViewShadow(view:UIView) {
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 0.8
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowRadius = 2
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func Tapped_Sidemenu() {
        self.slideMenuController()?.toggleLeft()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
