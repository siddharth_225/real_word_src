//
//  ViewController.swift
//  RealWorld
//
//  Created by MacBook Pro on 1/17/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var btn_next: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden=true
        self.btn_next.layer.borderWidth=1
        self.btn_next.layer.borderColor = UIColor.black.cgColor
        self.btn_next.layer.cornerRadius = self.btn_next.frame.size.height/2
        self.btn_next.layer.masksToBounds=true
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

