//
//  WalkThrougViewController1.swift
//  RealWorld
//
//  Created by MacBook Pro on 1/17/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit

class WalkThrougViewController1: UIViewController {

     @IBOutlet weak var btn_next: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btn_next.layer.borderWidth=1
        self.btn_next.layer.borderColor = UIColor.black.cgColor
        self.btn_next.layer.cornerRadius = self.btn_next.frame.size.height/2
        self.btn_next.layer.masksToBounds=true
        // Do any additional setup after loading the view.
    }

    @IBAction func Tapped_GetStarted(_ sender: Any) {
        
        self.performSegue(withIdentifier: "segue_dashboard", sender: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
