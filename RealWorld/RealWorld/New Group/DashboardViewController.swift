//
//  DashboardViewController.swift
//  RealWorld
//
//  Created by MacBook Pro on 1/17/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import KVNProgress

class DashboardViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var collectionview_category: UICollectionView!
    
    let arr_bg:[String] = ["img_launching","img_insurance","img_abad","img_stock","img_finance","img_realestate","img_ecommarce","img_ewallat","img_salt","img_realcola","img_realcoin","img_realchocolate"]
    let arr_icon:[String] = ["ic_launching","ic_insurance","ic_abad","ic_stock","ic_finance","ic_realestate","ic_ecom","ic_ewallet","ic_salt","ic_realcola","ic_realcoin","ic_realchocolate"]
    
    let arr_name:[String] = ["Launching","Insurance","Ahmedabad Film City","Stock Market","Finance","Real Estate","E Commerce","E Wallet","Salt","Real Cola","Real Coin","Real Chocolate"]
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()

        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "ic_sidemenu"), for: .normal)
        button.layer.cornerRadius = 5
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        button.addTarget(self, action: #selector(Tapped_Sidemenu), for: .touchUpInside)
        
        let lbl:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        lbl.font = UIFont.init(name: "Stilu-SemiBold", size: 15)
        lbl.text = "Categories"
        lbl.textColor = UIColor.black
        let bar_buttonlbl = UIBarButtonItem(customView: lbl)
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.leftBarButtonItems = [barButton,bar_buttonlbl]
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        
        self.collectionview_category.delegate=nil
        self.collectionview_category.dataSource=nil
        // Do any additional setup after loading the view.
    }
   
    override func viewDidAppear(_ animated: Bool) {
        self.settimer()
    }
    
    func settimer() {
     //   KVNProgress.dismiss()
        KVNProgress.show()
        timer.invalidate() // just in case this button is tapped multiple times
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction() {
            timer.invalidate()
            KVNProgress.dismiss()
        self.collectionview_category.delegate=self
        self.collectionview_category.dataSource=self
    }
    
    @objc func Tapped_Sidemenu() {
        self.slideMenuController()?.toggleLeft()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Collectionview delegate method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_bg.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let lbl_time:UILabel  = cell.contentView.viewWithTag(103) as! UILabel
        let img_bg:UIImageView  = cell.contentView.viewWithTag(101) as! UIImageView
        let img_icons:UIImageView  = cell.contentView.viewWithTag(102) as! UIImageView
        img_bg.image = UIImage(named: self.arr_bg[indexPath.row])
        img_icons.image = UIImage(named: self.arr_icon[indexPath.row])
        lbl_time.text = self.arr_name[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  30
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/3, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var str_url:String = ""
        switch indexPath.row {
        case 0:
            str_url = "http://realworld.co.in/ext/7-star-club/"
            break
        case 1:
            str_url = "http://realworld.co.in/ext/insurance/"
            break
        case 2:
            str_url = "http://realworld.co.in/ext/ahmedabad-film-city/"
            break
        case 3:
            str_url = "http://realworld.co.in/ext/stock-market/"
            break
        case 4:
            str_url = "http://realworld.co.in/ext/finance/"
            break
        case 5:
            str_url = "http://realworld.co.in/ext/real-estate/"
            break
        case 6:
            str_url = "http://realworld.co.in/ext/e-commerce/"
            break
        case 7:
            str_url =  "http://realworld.co.in/ext/e-wallet/"
            break
        case 8:
            str_url = "http://realworld.co.in/ext/salt/"
            break
        case 9:
            str_url = "http://realworld.co.in/ext/real-cola/"
            break
        case 10:
            str_url = "http://realworld.co.in/ext/real-coin/"
            break
        case 11:
            str_url = "http://realworld.co.in/ext/real-chocolate/"
            break
        case 12:
            str_url = "http://realworld.co.in/ext/real-chocolate"
            break
        default:
            break
        }

       // UIApplication.shared.openURL(URL(string: str_url)!)
   
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: str_url)!, options: [:]) { (success) in
                print("open")
            }
        } else {
            // Fallback on earlier versions
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
