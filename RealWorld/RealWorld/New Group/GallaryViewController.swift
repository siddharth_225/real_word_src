//
//  GallaryViewController.swift
//  RealWorld
//
//  Created by MacBook Pro on 1/19/18.
//  Copyright © 2018 MacBook Pro. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import KVNProgress

class GallaryViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionview_gallary: UICollectionView!
    
    let arr_images:[String] = ["1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg","5.jpg","6.jpg","7.jpg"]
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "ic_sidemenu"), for: .normal)
        button.layer.cornerRadius = 5
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.addTarget(self, action: #selector(Tapped_Sidemenu), for: .touchUpInside)
        
        let lbl:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 30))
        lbl.font = UIFont.init(name: "Stilu-SemiBold", size: 15)
        lbl.text = "Gallary"
        lbl.textColor = UIColor.black
        let bar_buttonlbl = UIBarButtonItem(customView: lbl)
        let barButton = UIBarButtonItem(customView: button)
        
        self.navigationItem.leftBarButtonItems = [barButton,bar_buttonlbl]
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        // Do any additional setup after loading the view.
        
        self.collectionview_gallary.delegate=nil
        self.collectionview_gallary.dataSource=nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.settimer()
    }
    
    func settimer() {
       
        KVNProgress.show()
        timer.invalidate() // just in case this button is tapped multiple times
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction() {
        timer.invalidate()
        KVNProgress.dismiss()
        self.collectionview_gallary.delegate=self
        self.collectionview_gallary.dataSource=self
    }
    @objc func Tapped_Sidemenu() {
        self.slideMenuController()?.toggleLeft()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Collectionview delegate method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let img_bg:UIImageView  = cell.contentView.viewWithTag(101) as! UIImageView
        img_bg.image = UIImage(named: self.arr_images[indexPath.row])
        cell.layoutIfNeeded()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.row == 0
        {
            return CGSize(width: collectionView.frame.size.width, height: 110)
        }
        else
        {
            let padding: CGFloat =  28
            let collectionViewSize = collectionView.frame.size.width - padding
            return CGSize(width: collectionViewSize/3, height: 110)
        }
        
    }

}
