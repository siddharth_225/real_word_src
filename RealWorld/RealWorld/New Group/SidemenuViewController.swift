//
//  SidemenuViewController.swift
//  The Mint Club
//
//  Created by SSCS on 18/12/17.
//  Copyright © 2017 SSCS. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

enum LeftMenu: Int {
    case profile = 0
    //    case logs
    case acceptInventory
    case usedInventory
    case inventoryQueue
    case contactUs
    case logout
}

protocol SideMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class SidemenuViewController: UIViewController,SideMenuProtocol,UITableViewDelegate,UITableViewDataSource {
    
//    var mainViewController: UITabBarController!
//    var profileNavVC: UINavigationController
//    var acceptInventoryNavVC: UINavigationController!
//    var inventoryQueueNavVC: UINavigationController!
    
    var arr_name:[String] = ["About Us","Service Request","Gallary","Contact Us"]
    @IBOutlet weak var tbl_sidemenu: UITableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
       // self.mainViewController = self.slideMenuController()?.mainViewController as! UITabBarController!
        self.tbl_sidemenu.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Private Method -
    
    func changeViewController(_ menu: LeftMenu) {
        
//        switch menu {
//
//        case .profile:
//            self.slideMenuController()?.changeMainViewController(self.profileNavVC, close: true)
//
//        case .acceptInventory:
//            self.slideMenuController()?.changeMainViewController(self.acceptInventoryNavVC, close: true)
//
//        case .inventoryQueue:
//            self.slideMenuController()?.changeMainViewController(self.inventoryQueueNavVC, close: true)
//        default:
//            break
//        }
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        self.slideMenuController()?.closeLeft()
        if indexPath.row == 0
        {
            let contactvc:AboutusViewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutusViewController") as! AboutusViewController
            let nav:UINavigationController = UINavigationController(rootViewController: contactvc)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        }
        else if indexPath.row == 1
        {
            let contactvc:DashboardViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            let nav:UINavigationController = UINavigationController(rootViewController: contactvc)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        }
        else if indexPath.row == 2
        {
            let contactvc:GallaryViewController = self.storyboard?.instantiateViewController(withIdentifier: "GallaryViewController") as! GallaryViewController
            let nav:UINavigationController = UINavigationController(rootViewController: contactvc)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        }
        else
        {
            let contactvc:ContactusViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactusViewController") as! ContactusViewController
            let nav:UINavigationController = UINavigationController(rootViewController: contactvc)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        }
     }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arr_name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    let cell:UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell"))!
        
        let lbl_name = cell.contentView.viewWithTag(101) as! UILabel
        cell.selectionStyle = .none
        lbl_name.text = self.arr_name[indexPath.row]
       
        return cell
    }
}
